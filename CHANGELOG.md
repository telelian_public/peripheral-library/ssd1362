# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 2.0.0 (2020-04-10)


### ⚠ BREAKING CHANGES

* **show:** optimize pixel transfer

### Features

* **migration from pyfront:** initial commit ([c20c805](https://gitlab.com/telelian/peripheral-library/pyfront-vs301/commit/c20c8055d227b83b01224a4cb918a3c6c2a44ed4))


### improvement

* **show:** optimize pixel's transfer ([705d84c](https://gitlab.com/telelian/peripheral-library/pyfront-vs301/commit/705d84c2484a72036fd6d9d3b0813a390bf2fd1e))
